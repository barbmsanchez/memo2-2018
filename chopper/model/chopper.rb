class Chopper

  def chop(n, array)
    if array.length > 0
      return array.find_index(n)
    else
      return -1
    end
  end

  def sum(array)
    if array.length ==0
      return 'vacio'
    else
      a=array.inject(0, &:+)
      if a <= 99
        enString=a.to_s.split(//).map{|chr| chr.to_i}
        ar = ['cero,','uno,','dos,','tres,','cuatro,','cinco,','seis,','siete,','ocho,','nueve,']
        myString = String.new
        for item in enString
          myString=myString+ ar[item]
        end
        return myString.chop
      else
        return 'demasiado grande'
      end
    end
  end
  
end
